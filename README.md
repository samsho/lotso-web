# lotso-web

#### 项目介绍
1. Lotso Web 是基于多个优秀的开源项目，高度整合封装而成 Java Web 应用基础平台
2. 目前基础版本具有四个模块：系统管理、机构管理、监控管理、批量任务

#### 软件架构
1. 后端框架

| 名称 | 描述  |
|:--------: | :-------- |
|核心框架| 	Spring Boot、Spring、Spring MVC |
|持久层  | 	MyBatis、[MyBatis-Plus](http://mp.baomidou.com/) |
|权限框架| 	Spring Security |
|缓存框架| 	Spring Cache、Redis |
|日志管理| 	SLF4J、LogBack |
|任务调度| 	Spring Task、Quartz |
|工具组件| 	Guava、Apache Commons、Jackson、LomBok、UserAgentUtils |

2. 前端框架

| 名称 | 描述  |
|:--------: | :-------- |
|模板引擎| 	[Thymeleaf](https://www.thymeleaf.org/) |
|核心框架  | 	[layui-v2.3.0](https://www.layui.com/)、 [layui-treetable](https://gitee.com/whvse/treetable-lay)、 [formSelects-v4](https://github.com/hnzzmsf/layui-formSelects)、 Jquery|
|其他组件| 	jquery.backstretch.js |

#### 项目结构

1. 后端
```
|-common                      // 基础模块
|     |-config                  // 配置
|     |-exception               // 基础异常
|     |-log                     // 日志组件
|     |-security                // 权限安全
|     |-utils                   // 工具组件
|
|-module                      // 业务模块
|     |-base                    // 基础模块
|     |-batch                   // 批量任务模块
|     |-monitor                 // 监控模块
|     |-organization            // 机构管理
|     |-system                  // 系统管理
|
|-WebApplication              // 启动类
```

2. 前端
```
|-mapper                      // Mybatis Mapper 文件
|     |-batch                   // 批量处理模块
|     |-system                  // 系统模块
|
|-static                      // 静态组件
|     |-global                  // 全局层
|           |-css                   // 样式
|           |-images                // 图片
|           |-plugins               // 第三方库
|     |-module                  // layui 模块
|           |-formSelects           // Select 下拉
|           |-iconPicker            // 图标选择
|           |-treetable             // 树状表格
|           |-admin.js              // admin模块
|           |-index.js              // index模块
|     |-pages                   // 页面层
|
|-templates                   // 模板页面
|     |-error                   // error模块
|     |-gen                     // 代码生成模板
|     |-index                   // index模块
|     |-layout                  // 页面布局
|     |-module                  // 业务模块（内部根据业务细分）
|     |-index.html              // 主界面
|     |-login.html              // 登陆界面
|
|-application-xxx.yml           // 配置文件
|-logback-spring.xml            // 日志配置
|-quartz.properties             // Quartz配置
```


#### 运行体验

1. 具备运行环境：JDK1.8+、Maven3.0+、MySql5.7+
2. 插件要求：lombok(必须)、mybatis(提高开发效率)
3. 根据 doc/sql/web-mysql.sql 文件创建数据库与表数据
4. 根据自己创建的数据库账号密码，修改 src\main\resources\application-dev.yml 配置文件中相应参数
5. 设置运行环境  profiles active 为 dev
6. 运行启动：方法运行入口类 com.lotso.web.WebApplication
7. 管理员账号（用户名/密码）：admin/admin

#### 运行效果
1. 登录
2. 文档首页
3. 用户模块
4. 批量任务
