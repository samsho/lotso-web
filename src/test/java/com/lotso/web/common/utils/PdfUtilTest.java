package com.lotso.web.common.utils;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: PdfUtilTest
 * Description:
 * Date: 2019/5/8 10:43 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class PdfUtilTest {

    @Test
    public void createPdf() throws Exception {
        PdfUtil.create("D://Hello.pdf", "Hello Pdf");
    }

    @Test
    public void create() throws Exception {
        PdfUtil.PdfContent content = new PdfUtil.PdfContent();
        content.setAuthor("Sam");
        content.setCreator("Creator Sam");
        content.setTitle("PDF添加文件属性");
        content.setSubject("文件属性");

        content.setContent("展示如何给PDF文件设置各种属性，如作者名字，创建日期，创建者，或者标题。");

        PdfUtil.create("D://PDF-Attributes.pdf", content);
    }

    @Test
    public void create1() throws Exception {
        Map<String, Object> variables = new HashMap<>();
        variables.put("title", "用户列表");
        variables.put("name", "你好");
        variables.put("age", 19);
        variables.put("sex", 1);
        variables.put("url", "https://blog.csdn.net/u012228718");
        variables.put("img", "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1557375735474&di=48911db72579ef54efc031f4b7818bc2&imgtype=0&src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F20182%2F21%2F2018221142159_MZ33z.jpeg");

        PdfUtil.create("D://PDF-Template.pdf", "/templates/pdf/pdf_template.ftl", variables);
    }

}