package com.lotso.web.common.config;

import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * ClassName: WebSecurityConfigTest
 * Description:
 * Date: 2018/9/1 11:11 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class WebSecurityConfigTest {

    @Test
    public void encoder() {
        long start = System.currentTimeMillis();
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder(4);
        String result = passwordEncoder.encode("N2M0YThkMDljYTM3NjJhZjYxZTU5NTIwOTQzZGMyNjQ5NGY4OTQxYg==");
        System.out.println((System.currentTimeMillis() - start) + "   " + result);
    }

    @Test
    public void pwd() {
        long start = System.currentTimeMillis();
        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder(4);
        String result = passwordEncoder.encode("superadmin");
        System.out.println((System.currentTimeMillis() - start) + "   " + result);
    }

}