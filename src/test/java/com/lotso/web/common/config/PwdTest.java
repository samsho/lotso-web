package com.lotso.web.common.config;

import org.jasypt.encryption.StringEncryptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * ClassName: PwdTest
 * Description: 数据库密码配置加密
 * Date: 2018/9/10 10:03 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dat")
public class PwdTest {

    @Autowired
    StringEncryptor stringEncryptor;

    /**
     * 方式一，使用
     */
    @Test
    public void test() {
        String result = stringEncryptor.encrypt("pwd");
        System.out.println(result);
    }

    /**
     * 方式二
     * input:是数据库的明文密码
     * password：是机密的盐
     * algorithm：是加密的方式(默认)
     */
    @Test
    public void test2() {
        String result = "java -cp C:\\Users\\home\\.m2\\repository\\org\\jasypt\\jasypt\\1.9.2\\jasypt-1.9.2.jar org.jasypt.intf.cli.JasyptPBEStringEncryptionCLI input=\"pwd\" password=lotsoPwdSail algorithm=PBEWithMD5AndDES";
    }
}