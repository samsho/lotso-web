layui.define(['jquery', 'layer'], function (exports) {
    let $ = layui.jquery;
    let layer = layui.layer;
    let popupRightIndex, popupCenterIndex, popupCenterParam;

    let admin = {
        isRefresh: false,
        // 设置侧栏折叠
        flexible: function (expand) {
            let $layui_admin = $('.layui-layout-admin');
            const isExpand = $layui_admin.hasClass('admin-nav-mini');
            if (isExpand === !expand) {
                return;
            }
            if (expand) {
                $layui_admin.removeClass('admin-nav-mini');
            } else {
                $layui_admin.addClass('admin-nav-mini');
            }
        },
        // 设置导航栏选中
        activeNav: function (url) {
            let $layui_item = $('.layui-layout-admin .layui-side .layui-nav .layui-nav-item');
            $('.layui-layout-admin .layui-side .layui-nav .layui-nav-item .layui-nav-child dd').removeClass('layui-this');
            $layui_item.removeClass('layui-this');
            if (url && url !== '') {
                $layui_item.removeClass('layui-nav-itemed');
                const $a = $('.layui-layout-admin .layui-side .layui-nav a[href="#!' + url + '"]');
                $a.parent('li').addClass('layui-this');
                $a.parent('dd').addClass('layui-this');
                $a.parent('dd').parent('.layui-nav-child').parents('.layui-nav-item').addClass('layui-nav-itemed');
                $a.parent('dd').parent('.layui-nav-child').parent('dd').addClass('layui-nav-itemed');
            }
        },
        // 刷新主题部分
        refresh: function () {
            admin.isRefresh = true;
            Q.refresh();
        },
        // 右侧弹出（参数详见 https://www.layui.com/doc/modules/layer.html#anim）
        popupRight: function (path) {
            const param = {};
            param.path = path;
            param.id = 'adminPopupR';
            param.title = false;
            param.anim = 2;
            param.isOutAnim = false;
            param.closeBtn = false;
            param.offset = 'r';
            param.shadeClose = true;
            param.area = '336px';
            param.skin = 'layui-layer-adminRight';
            param.end = function () {
                layer.closeAll('tips');
            };
            popupRightIndex = admin.open(param);
            return popupRightIndex;
        },
        // 关闭右侧弹出
        closePopupRight: function () {
            layer.close(popupRightIndex);
        },
        // 中间弹出
        popupCenter: function (param) {
            param.id = 'adminPopupC';
            popupCenterParam = param;
            popupCenterIndex = admin.open(param);
            return popupCenterIndex;
        },
        // 关闭中间弹出并且触发finish回调
        finishPopupCenter: function () {
            layer.close(popupCenterIndex);
            popupCenterParam.finish ? popupCenterParam.finish() : '';
        },
        // 关闭中间弹出
        closePopupCenter: function () {
            layer.close(popupCenterIndex);
        },
        // 封装layer.open
        open: function (param) {
            const sCallBack = param.success;
            param.type = 1;
            param.area = param.area ? param.area : '450px';
            param.offset = param.offset ? param.offset : '80px';
            param.resize = param.resize ? param.resize : false;
            param.shade = param.shade ? param.shade : .2;
            param.success = function (layero, index) {
                sCallBack ? sCallBack(layero, index) : '';
                admin.ajax({
                    url: param.path,
                    type: 'GET',
                    dataType: 'html',
                    index: index,
                    success: function (result, status, xhr) {
                        $(layero).children('.layui-layer-content').html(result);
                    }
                });
            };
            return layer.open(param);
        },
        // 封装ajax请求，返回数据类型为json
        reqJson: function (url, data, success, method) {
            admin.ajax({
                url: url,
                data: data,
                type: method,
                dataType: 'json',
                success: success
            });
        },
        // 封装ajax请求
        ajax: function (param) {
            const successCallback = param.success;
            param.success = function (result, status, xhr) {
                // 判断登录过期和没有权限
                let jsonRs;
                if ('json' === param.dataType.toLowerCase()) {
                    jsonRs = result;
                } else if ('html' === param.dataType.toLowerCase() || 'text' === param.dataType.toLowerCase()) {
                    jsonRs = admin.parseJSON(result);
                }
                if (jsonRs) {
                    if (jsonRs.code === 401) {
                        layer.msg(jsonRs.msg, {icon: 2, time: 1500}, function () {
                            location.replace(basePath + 'login');
                        }, 1000);
                        return;
                    } else if ('html' === param.dataType.toLowerCase() && jsonRs.code === 403) {
                        layer.msg(jsonRs.msg, {icon: 2});
                    } else {
                        layer.close(param.index);
                        layer.msg(jsonRs.msg, {icon: 2});
                    }
                }
                successCallback(result, status, xhr);
            };
            param.error = function (xhr) {
                const responseText = xhr.responseText;
                const status = xhr.status;
                if (status === 401) {
                    const jsonResult = admin.parseJSON(responseText);
                    layer.msg(jsonResult.msg, {icon: 2, time: 1500}, function () {
                        location.replace(basePath + 'login');
                    }, 100);
                    return;
                } else if (status === 403 || status === 404 || status === 500) {
                    param.success(responseText);
                } else {
                    param.success({code: status, msg: xhr.statusText});
                }
            };
            $.ajax(param);
        },
        // 显示加载动画
        showLoading: function (element) {
            $(element).append('<i class="layui-icon layui-icon-loading layui-anim layui-anim-rotate layui-anim-loop admin-loading"></i>');
        },
        // 移除加载动画
        removeLoading: function (element) {
            $(element + '>.admin-loading').remove();
        },
        // 缓存临时数据
        putTempData: function (key, value) {
            if (value) {
                layui.sessionData('tempData', {key: key, value: value});
            } else {
                layui.sessionData('tempData', {key: key, remove: true});
            }
        },
        // 获取缓存临时数据
        getTempData: function (key) {
            return layui.sessionData('tempData')[key];
        },
        // 保存持久化存储
        putLocalData: function (key, value) {
            if (value) {
                layui.data('localData', {key: key, value: value});
            } else {
                layui.data('localData', {key: key, remove: true});
            }
        },
        getLocalData: function (key) {
            return layui.data('localData')[key];
        },

        // 滑动选项卡
        rollPage: function (d) {
            const $tabTitle = $('.layui-layout-admin .layui-body .layui-tab .layui-tab-title');
            const left = $tabTitle.scrollLeft();
            if ('left' === d) {
                $tabTitle.scrollLeft(left - 120);
            } else if ('auto' === d) {
                let autoLeft = 0;
                $tabTitle.children("li").each(function () {
                    if ($(this).hasClass('layui-this')) {
                        return false;
                    } else {
                        autoLeft += $(this).outerWidth();
                    }
                });
                $tabTitle.scrollLeft(autoLeft - 47);
            } else {
                $tabTitle.scrollLeft(left + 120);
            }
        },
        // 判断是否为json
        parseJSON: function (str) {
            if (typeof str === 'string') {
                try {
                    const obj = JSON.parse(str);
                    if (typeof obj === 'object' && obj) {
                        return obj;
                    }
                } catch (e) {
                }
            }
        }
    };

    // Admin提供的事件
    admin.events = {
        // 折叠侧导航
        flexible: function (e) {
            let expand = $('.layui-layout-admin').hasClass('admin-nav-mini');
            admin.flexible(expand);
        },
        // 刷新主体部分
        refresh: function () {
            admin.refresh();
        },
        //后退
        back: function () {
            history.back();
        },
        // 全屏
        fullScreen: function (e) {
            let ws;
            let ac = 'layui-icon-screen-full', ic = 'layui-icon-screen-restore';
            let ti = $(this).find('i');

            let isFullscreen = document.fullscreenElement || document.msFullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement || false;
            if (isFullscreen) {
                let efs = document.exitFullscreen || document.webkitExitFullscreen || document.mozCancelFullScreen || document.msExitFullscreen;
                if (efs) {
                    efs.call(document);
                } else if (window.ActiveXObject) {
                    ws = new ActiveXObject('WScript.Shell');
                    ws && ws.SendKeys('{F11}');
                }
                ti.addClass(ac).removeClass(ic);
            } else {
                let el = document.documentElement;
                let rfs = el.requestFullscreen || el.webkitRequestFullscreen || el.mozRequestFullScreen || el.msRequestFullscreen;
                if (rfs) {
                    rfs.call(el);
                } else if (window.ActiveXObject) {
                    ws = new ActiveXObject('WScript.Shell');
                    ws && ws.SendKeys('{F11}');
                }
                ti.addClass(ic).removeClass(ac);
            }
        },
        // 左滑动tab
        leftPage: function () {
            admin.rollPage("left");
        },
        // 右滑动tab
        rightPage: function () {
            admin.rollPage();
        },
        // 关闭当前选项卡
        closeThisTabs: function () {
            var $title = $('.layui-layout-admin .layui-body .layui-tab .layui-tab-title');
            if ($title.find('li').first().hasClass('layui-this')) {
                return;
            }
            $title.find('li.layui-this').find(".layui-tab-close").trigger("click");
        },
        // 关闭其他选项卡
        closeOtherTabs: function () {
            $('.layui-layout-admin .layui-body .layui-tab .layui-tab-title li:gt(0):not(.layui-this)').find('.layui-tab-close').trigger('click');
        },
        // 关闭所有选项卡
        closeAllTabs: function () {
            $('.layui-layout-admin .layui-body .layui-tab .layui-tab-title li:gt(0)').find('.layui-tab-close').trigger('click');
        },
        // 关闭所有弹窗
        closeDialog: function () {
            layer.closeAll('page');
        },
        // 便签
        note: function () {
            admin.popupCenter({
                title: '便签',
                path: basePath + 'index/note',
                area: '300px',
                offset: ['50px', '65%'],
                cancel: function (index, layero) {
                    let t_note = $("#index_note textarea").val();
                    admin.putLocalData("note", t_note);
                }
            });
        }

    };

    // 所有 event click 事件监听
    $('body').on('click', '*[app-event]', function () {
        const eventName = $(this).attr('app-event');
        const event = admin.events[eventName];
        event && event.call(this, $(this));
    });

    // 移动设备遮罩层点击事件（元素在 footer.html）
    $('.site-mobile-shade').click(function () {
        admin.flexible(true);
    });

    // 侧导航折叠状态下鼠标经过显示提示
    $('body').on('mouseenter', '.layui-layout-admin.admin-nav-mini .layui-side .layui-nav .layui-nav-item>a', function () {
        const tipText = $(this).find('cite').text();
        if (document.body.clientWidth > 750) {
            layer.tips(tipText, this);
        }
    }).on('mouseleave', '.layui-layout-admin.admin-nav-mini .layui-side .layui-nav .layui-nav-item>a', function () {
        layer.closeAll('tips');
    });

    // 侧导航折叠状态下点击展开
    $('body').on('click', '.layui-layout-admin.admin-nav-mini .layui-side .layui-nav .layui-nav-item>a', function () {
        if (document.body.clientWidth > 750) {
            layer.closeAll('tips');
            admin.flexible(true);
        }
    });

    // 所有lay-tips处理
    $('body').on('mouseenter', '*[lay-tips]', function () {
        const tipText = $(this).attr('lay-tips');
        const dt = $(this).attr('lay-direction');
        layer.tips(tipText, this, {tips: dt || 1, time: -1});
    }).on('mouseleave', '*[lay-tips]', function () {
        layer.closeAll('tips');
    });

    exports('admin', admin);
});
