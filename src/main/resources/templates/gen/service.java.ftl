package ${package.Service};

import ${package.Entity}.${entity};
import ${superServiceClassPackage};

/**
 * ClassName： ${table.serviceName}
 * Description：${table.comment} 服务类
 * Date: ${date} 【需求编号】
 *
 * @author ${author}
 * @version V1.0.0
 */
<#if kotlin>
interface ${table.serviceName} : ${superServiceClass}<${entity}>
<#else>
public interface ${table.serviceName} extends ${superServiceClass}<${entity}> {

}
</#if>
