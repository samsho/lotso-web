package ${package.Mapper};

import ${package.Entity}.${entity};
import ${superMapperClassPackage};
import org.springframework.stereotype.Repository;

/**
 * ClassName： ${table.mapperName}
 * Description：${table.comment} Mapper 接口
 * Date: ${date} 【需求编号】
 *
 * @author ${author}
 * @version V1.0.0
 */
<#if kotlin>
interface ${table.mapperName} : ${superMapperClass}<${entity}>
<#else>
@Repository
public interface ${table.mapperName} extends ${superMapperClass}<${entity}> {

}
</#if>
