package com.lotso.web.module.organization.service;

import com.baomidou.mybatisplus.service.IService;
import com.lotso.web.common.ReqParam;
import com.lotso.web.module.organization.entity.Office;

import java.util.List;

/**
 * ClassName: IOfficeService
 * Description: 机构业务
 * Date: 2018/9/2 9:16 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public interface IOfficeService extends IService<Office> {

    /**
     * 查询列表
     *
     * @return
     */
    List<Office> list();

    /**
     * 根据条件查询
     *
     * @param reqParam
     * @return
     */
    List<Office> list(ReqParam reqParam);


    /**
     * 添加
     *
     * @param office
     * @return
     */
    boolean add(Office office);

    /**
     * 更新
     *
     * @param office
     * @return
     */
    boolean update(Office office);

    /**
     * 删除
     *
     * @param officeId
     * @return
     */
    boolean delete(String officeId);

    /**
     * 获取用户有权限的机构信息（所属机构以及下级机构）
     *
     * @param type
     * @return
     */
    List<Office> authGet(String type);
}
