package com.lotso.web.module.organization.controller;

import com.lotso.web.common.Result;
import com.lotso.web.module.base.AbstractController;
import com.lotso.web.module.organization.entity.Area;
import com.lotso.web.module.organization.entity.Office;
import com.lotso.web.module.organization.service.impl.AreaServiceImpl;
import com.lotso.web.module.organization.service.impl.OfficeServiceImpl;
import com.lotso.web.module.system.service.impl.DictServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * ClassName: OfficeController
 * Description: 机构管理
 * Date: 2018/8/30 10:51 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Controller
@RequestMapping("/organization/office")
public class OfficeController extends AbstractController {

    @Autowired
    private OfficeServiceImpl officeService;

    @Autowired
    private AreaServiceImpl areaService;

    @Autowired
    private DictServiceImpl dictService;

    /**
     * 进入展示页面
     *
     * @return
     */
    @PreAuthorize("hasAuthority('office:view')")
    @GetMapping
    public String office() {
        return "module/organization/office";
    }

    /**
     * 查询列表
     **/
    @PreAuthorize("hasAuthority('office:view')")
    @ResponseBody
    @GetMapping("/list")
    public Result<Office> list() {
        List<Office> offices = officeService.list();
        return new Result<>(offices);
    }


    /**
     * 进入添加|修改页面
     *
     * @param model
     * @return
     */
    @PreAuthorize("hasAuthority('office:add')")
    @GetMapping("editForm")
    public String editForm(Model model) {
        List<Office> offices = officeService.list();
        model.addAttribute("offices", offices);

        List<Area> areas = areaService.list();
        model.addAttribute("areas", areas);

        return "module/organization/office_form";
    }

    /**
     * 添加操作
     */
    @PreAuthorize("hasAuthority('office:add')")
    @ResponseBody
    @PostMapping("/add")
    public Result add(Office office) {
        if (officeService.add(office)) {
            return Result.ok("添加成功");
        }
        return Result.error("添加失败");
    }

    /**
     * 修改菜单
     */
    @PreAuthorize("hasAuthority('office:edit')")
    @ResponseBody
    @PostMapping("/update")
    public Result update(Office office) {
        if (officeService.update(office)) {
            return Result.ok("修改成功");
        }
        return Result.error("修改失败");
    }

    /**
     * 删除菜单
     */
    @PreAuthorize("hasAuthority('office:delete')")
    @ResponseBody
    @PostMapping("/delete")
    public Result delete(String officeId) {
        if (officeService.delete(officeId)) {
            return Result.ok("删除成功");
        }
        return Result.error("删除失败");
    }


    /**
     * 获取用户所属机构以及下级机构
     *
     * @return
     */
    @PreAuthorize("hasAuthority('office:view')")
    @ResponseBody
    @PostMapping("/get/{type}")
    public Result get(@PathVariable("type") String type) {
        List<Office> offices = officeService.authGet(type);
        return new Result(offices);
    }

}
