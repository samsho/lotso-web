package com.lotso.web.module.organization.controller;

import com.lotso.web.common.Result;
import com.lotso.web.module.base.AbstractController;
import com.lotso.web.module.organization.entity.Area;
import com.lotso.web.module.organization.service.impl.AreaServiceImpl;
import com.lotso.web.module.system.service.impl.DictServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * ClassName: AreaController
 * Description: 区域管理
 * Date: 2018/8/30 10:51 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Controller
@RequestMapping("/organization/area")
public class AreaController extends AbstractController {

    @Autowired
    private AreaServiceImpl areaService;

    @Autowired
    private DictServiceImpl dictService;

    /**
     * 进入展示页面
     *
     * @return
     */
    @PreAuthorize("hasAuthority('area:view')")
    @RequestMapping
    public String area() {
        return "module/organization/area";
    }

    /**
     * 查询列表
     **/
    @PreAuthorize("hasAuthority('area:view')")
    @ResponseBody
    @RequestMapping("/list")
    public Result<Area> list() {
        List<Area> areas = areaService.list();
        return new Result<>(areas);
    }


    /**
     * 进入添加|修改页面
     *
     * @param model
     * @return
     */
    @PreAuthorize("hasAuthority('area:add')")
    @RequestMapping("editForm")
    public String editForm(Model model) {
        List<Area> areas = areaService.list();
        model.addAttribute("areas", areas);
        return "module/organization/area_form";
    }

    /**
     * 添加操作
     */
    @PreAuthorize("hasAuthority('area:add')")
    @ResponseBody
    @RequestMapping("/add")
    public Result add(Area area) {
        if (areaService.add(area)) {
            return Result.ok("添加成功");
        }
        return Result.error("添加失败");
    }

    /**
     * 修改菜单
     */
    @PreAuthorize("hasAuthority('area:edit')")
    @ResponseBody
    @RequestMapping("/update")
    public Result update(Area area) {
        if (areaService.update(area)) {
            return Result.ok("修改成功");
        }
        return Result.error("修改失败");
    }

    /**
     * 删除菜单
     */
    @PreAuthorize("hasAuthority('area:delete')")
    @ResponseBody
    @RequestMapping("/delete")
    public Result delete(String areaId) {
        if (areaService.delete(areaId)) {
            return Result.ok("删除成功");
        }
        return Result.error("删除失败");
    }

}
