package com.lotso.web.module.organization.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.lotso.web.module.base.AbstractDataEntity;
import com.lotso.web.module.system.entity.User;
import lombok.Getter;
import lombok.Setter;

/**
 * ClassName: Office
 * Description: 机构
 * Date: 2018/9/27 18:36 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */

@Getter
@Setter
@TableName("t_org_office")
public class Office extends AbstractDataEntity {

    /**
     * 归属区域
     *
     * @TableField(value="areaId",el="id") private Area area
     * 这样也可以处理，但是前端修改数据回显比较麻烦，级联查询也需要自己手动写sql
     */
    private String areaId;

    /**
     * 机构编码
     */
    private String code;
    /**
     * 机构名称
     */
    private String name;

    /**
     * 机构类型（1：公司；2：部门；3：小组；4：其他）
     */
    private String type;

    /**
     * 机构等级（1：一级；2：二级；3：三级；4：四级）
     */
    private String grade;

    /**
     * 联系地址
     */
    private String address;

    /**
     * 邮政编码
     */
    private String zipCode;

    /**
     * 电话
     */
    private String phone;

    /**
     * 传真
     */
    private String fax;

    /**
     * 负责人
     */
    private String master;

    /**
     * 主负责人
     */
    private String primaryUser;
    @TableField(exist = false)
    private User pUser;

    /**
     * 副负责人
     */
    private String deputyUser;
    @TableField(exist = false)
    private User dUser;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 备注
     */
    private String description;

    /**
     * 上级机构
     */
    private String parentId;

}
