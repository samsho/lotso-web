package com.lotso.web.module.organization.service;

import com.lotso.web.module.organization.entity.Area;

import java.util.List;

/**
 * ClassName: IAreaService
 * Description: 区域业务
 * Date: 2018/9/2 9:16 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public interface IAreaService {

    /**
     * 查询所有的区域列表
     *
     * @return
     */
    List<Area> list();


    /**
     * 添加区域
     *
     * @param area
     * @return
     */
    boolean add(Area area);

    /**
     * 更新区域
     *
     * @param area
     * @return
     */
    boolean update(Area area);

    /**
     * 删除区域
     *
     * @param areaId
     * @return
     */
    boolean delete(String areaId);

}
