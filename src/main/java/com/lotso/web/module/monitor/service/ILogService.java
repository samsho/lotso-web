package com.lotso.web.module.monitor.service;

import com.lotso.web.common.ReqParam;
import com.lotso.web.common.Result;
import com.lotso.web.module.monitor.entity.Log;

import java.util.List;

/**
 * ClassName: ILogService
 * Description:
 * Date: 2018/9/2 9:16 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public interface ILogService {

    /**
     * 批量保存
     *
     * @param logs
     * @return
     */
    boolean addBatch(List<Log> logs);

    /**
     * @param reqParam
     * @param username
     * @return
     */
    Result<Log> list(ReqParam reqParam, String username);
}
