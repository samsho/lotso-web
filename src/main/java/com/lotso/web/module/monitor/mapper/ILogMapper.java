package com.lotso.web.module.monitor.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.lotso.web.module.base.IBaseMapper;
import com.lotso.web.module.monitor.entity.Log;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ClassName: ILogMapper
 * Description: 日志
 * Date: 2018/9/2 9:16 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Repository
public interface ILogMapper extends IBaseMapper<Log> {

    /**
     * 查询日志列表
     *
     * @param page      分页
     * @param startDate 开始时间
     * @param endDate   结束时间
     * @param username  用户
     * @return
     */
    List<Log> listFull(Page<Log> page, @Param("startDate") String startDate, @Param("endDate") String endDate, @Param("username") String username);

    /**
     * 批量保存日志
     *
     * @param logs
     * @return
     */
    int addBatch(@Param("logs") List<Log> logs);

    /**
     * 批量保存日志（Oracle）
     *
     * @param logs
     * @return
     */
    int addBatchForOracle(@Param("logs") List<Log> logs);
}
