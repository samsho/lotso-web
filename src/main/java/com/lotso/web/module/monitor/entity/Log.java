package com.lotso.web.module.monitor.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.lotso.web.module.base.AbstractBaseEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * ClassName: Log
 * Description: 系统日志
 * Date: 2018/8/30 10:51 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Setter
@Getter
@TableName("sys_log")
public class Log extends AbstractBaseEntity {

    /**
     * 用户账号
     */
    private String username;

    /**
     * 访问地址
     */
    private String url;

    /**
     * 操作动作
     */
    private String action;

    /**
     * 具体参数
     */
    private String args;

    /**
     * 异常信息
     */
    private String exception;

    /**
     * 操作系统
     */
    private String osName;

    /**
     * 设备型号
     */
    private String device;

    /**
     * 浏览器类型
     */
    private String browserType;

    /**
     * ip地址
     */
    private String ip;

    /**
     * 操作时间
     */
    private Date createTime = new Date();


}