package com.lotso.web.module.monitor.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.lotso.web.common.ReqParam;
import com.lotso.web.common.Result;
import com.lotso.web.common.exception.BusinessException;
import com.lotso.web.module.monitor.entity.Log;
import com.lotso.web.module.monitor.mapper.ILogMapper;
import com.lotso.web.module.monitor.service.ILogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * ClassName: LogServiceImpl
 * Description:
 * Date: 2018/9/2 9:16 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class LogServiceImpl extends ServiceImpl<ILogMapper, Log> implements ILogService {

    @Autowired
    private ILogMapper logMapper;

    @Override
    public boolean addBatch(List<Log> logs) {
        if (!logs.isEmpty() && (logMapper.addBatch(logs) < logs.size())) {
            throw new BusinessException("操作失败");
        }
        return true;
    }

    @Transactional(rollbackFor = Exception.class, readOnly = true)
    @Override
    public Result<Log> list(ReqParam reqParam, String username) {
        Page<Log> page = new Page<>(reqParam.getPage(), reqParam.getLimit());
        List<Log> records = logMapper.listFull(page, reqParam.getStartDate(), reqParam.getEndDate(), username);
        return new Result<>(page.getTotal(), records);
    }
}