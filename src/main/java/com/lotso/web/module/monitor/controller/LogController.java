package com.lotso.web.module.monitor.controller;

import com.lotso.web.common.ReqParam;
import com.lotso.web.common.Result;
import com.lotso.web.module.monitor.entity.Log;
import com.lotso.web.module.monitor.service.impl.LogServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * ClassName: LogController
 * Description: 日志管理
 * Date: 2018/8/30 10:51 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Controller
@RequestMapping("/monitor/log")
public class LogController {

    @Autowired
    private LogServiceImpl logService;

    @PreAuthorize("hasAuthority('log:view')")
    @RequestMapping()
    public String log() {
        return "module/monitor/log";
    }

    /**
     * 查询所有登录日志
     **/
    @PreAuthorize("hasAuthority('log:view')")
    @ResponseBody
    @RequestMapping("/list")
    public Result<Log> list(ReqParam reqParam, String username) {
        return logService.list(reqParam, username);
    }

}
