package com.lotso.web.module.monitor.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * ClassName: ChartsController
 * Description: 统计监控
 * Date: 2018/9/6 9:42 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Controller
@RequestMapping("monitor/charts")
public class ChartsController {

    @PreAuthorize("hasAuthority('charts:view')")
    @RequestMapping
    public String list() {
        return "module/monitor/charts";
    }
}
