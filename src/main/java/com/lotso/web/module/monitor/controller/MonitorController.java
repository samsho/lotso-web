package com.lotso.web.module.monitor.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * ClassName: MonitorController
 * Description:
 * Date: 2018/9/6 9:42 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Controller
@RequestMapping("monitor")
public class MonitorController {


    /**
     * iframe 页展示，可以用于集成监控等页面，如 druid、redis
     */
    @PreAuthorize("hasAuthority('druid:view')")
    @RequestMapping("/druid")
    public String error(Model model) {
        model.addAttribute("url", "druid");
        return "index/iframe";
    }
}
