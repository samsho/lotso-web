package com.lotso.web.module.base;

import com.lotso.web.common.utils.UserUtil;
import org.springframework.security.core.userdetails.UserDetails;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * ClassName: BaseController
 * Description: Controller 基类
 * Date: 2018/8/30 10:51 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public abstract class AbstractController {

    @Resource
    protected HttpServletRequest request;

    /**
     * 向Session 存放数据
     *
     * @param key
     * @param value
     */
    protected void setSessionAttr(String key, Object value) {
        HttpSession session = request.getSession(true);
        session.setAttribute(key, value);
    }

    /**
     * 从Session 获取数据
     *
     * @param key
     * @return
     */
    protected Object getSessionAttr(String key) {
        HttpSession session = request.getSession(true);
        return session.getAttribute(key);
    }

    protected UserDetails getPrincipal() {
        return UserUtil.getPrincipal();
    }

    protected String getPrincipalName() {
        return UserUtil.getPrincipalName();
    }


}
