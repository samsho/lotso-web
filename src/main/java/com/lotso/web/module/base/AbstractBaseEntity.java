package com.lotso.web.module.base;

import com.baomidou.mybatisplus.annotations.TableId;
import lombok.Getter;
import lombok.Setter;

/**
 * ClassName: AbstractBaseEntity
 * Description: Entity 基类
 * Date: 2018/9/2 7:51 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Setter
@Getter
public abstract class AbstractBaseEntity {

    public static final int NORMAL_STATE = 0;
    public static final int NOT_NORMAL_STATE = 1;

    /**
     * 实体编号（唯一标识）
     * 主键的处理，在配置文件中全局配置
     * 主键类型  0:"数据库ID自增", 1:"用户输入ID",2:"全局唯一ID (数字类型唯一ID)", 3:"全局唯一ID UUID";
     */
    @TableId
    protected String id;

    public AbstractBaseEntity() {
    }

    public AbstractBaseEntity(String id) {
        this.id = id;
    }
}
