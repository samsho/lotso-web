package com.lotso.web.module.batch.job.impl;

import com.alibaba.fastjson.JSON;
import com.lotso.web.module.batch.entity.BatchSchedule;
import com.lotso.web.module.batch.job.AbstractJobBean;
import com.lotso.web.module.batch.service.impl.BatchScheduleServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * ClassName: SimpleJob
 * Description:
 * Date: 2018/9/10 15:24 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Slf4j
public class SimpleJob2 extends AbstractJobBean {

    /**
     * 直接可以注入
     */
    @Autowired
    private BatchScheduleServiceImpl batchScheduleService;

    @Override
    protected void doData(JobExecutionContext context) throws JobExecutionException {

        String str = (String) context.getMergedJobDataMap().get(BatchSchedule.SCHEDULE_KEY);
        BatchSchedule scheduleJob = JSON.parseObject(str, BatchSchedule.class);
        
        log.info(" {} ************ 任务执行中 ************", scheduleJob.toString());

    }
}
