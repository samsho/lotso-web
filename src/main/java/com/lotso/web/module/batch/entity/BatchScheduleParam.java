package com.lotso.web.module.batch.entity;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.Getter;
import lombok.Setter;

/**
 * ClassName: BatchScheduleParam
 * Description: 任务计划参数
 * Date: 2018/9/11 9:32 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Setter
@Getter
@TableName("t_batch_schedule_param")
public class BatchScheduleParam {

    /**
     * 任务计划ID
     */
    private String scheduleId;

    /**
     * 任务计划code
     */
    private String scheduleCode;

    /**
     * 参数名
     */
    private String paramName;

    /**
     * 参数值
     */
    private String paramValue;

    /**
     * 动态参数使用
     */
    private String tempId;

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }

    public BatchScheduleParam() {
    }

    public BatchScheduleParam(String tempId, String paramName, String paramValue, String LAY_TABLE_INDEX) {
        this.paramName = paramName;
        this.paramValue = paramValue;
    }
}
