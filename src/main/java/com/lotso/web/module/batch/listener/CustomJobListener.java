package com.lotso.web.module.batch.listener;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.listeners.JobListenerSupport;

/**
 * ClassName: CusJobListener
 * Description:
 * Date: 2018/9/10 13:13 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class CustomJobListener extends JobListenerSupport {
    @Override
    public String getName() {
        return this.getClass().getName();
    }

    @Override
    public void jobToBeExecuted(JobExecutionContext context) {
        getLog().info("0");
    }

    @Override
    public void jobExecutionVetoed(JobExecutionContext context) {
        getLog().info("1");
    }

    @Override
    public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
        getLog().info("3");
    }
}
