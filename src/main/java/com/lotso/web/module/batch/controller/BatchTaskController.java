package com.lotso.web.module.batch.controller;

import com.lotso.web.common.ReqParam;
import com.lotso.web.common.Result;
import com.lotso.web.module.base.AbstractController;
import com.lotso.web.module.batch.entity.BatchTask;
import com.lotso.web.module.batch.service.impl.BatchTaskServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * ClassName: BatchTaskController
 * Description: 基本任务管理
 * Date: 2017/12/27 15:36 【需求编号】
 *
 * @author Shaom
 * @version V1.0.0
 */
@Controller
@RequestMapping("/batch/task")
public class BatchTaskController extends AbstractController {

    @Autowired
    private BatchTaskServiceImpl taskService;

    /**
     * 进入展示页面
     *
     * @return
     */
    @PreAuthorize("hasAuthority('task:view')")
    @GetMapping
    public String user() {
        return "module/batch/task";
    }

    /**
     * 分页查询列表
     */
    @PreAuthorize("hasAuthority('task:view')")
    @ResponseBody
    @GetMapping("/list")
    public Result<BatchTask> list(ReqParam reqParam) {
        return taskService.list(reqParam);
    }

    /**
     * 进入添加或者修改页面
     *
     * @return
     */
    @PreAuthorize("hasAuthority('task:add')")
    @RequestMapping("/editForm")
    public String editForm() {
        return "module/batch/task_form";
    }

    /**
     * 添加任务
     **/
    @PreAuthorize("hasAuthority('task:add')")
    @ResponseBody
    @RequestMapping("/add")
    public Result add(BatchTask task) {
        if (taskService.add(task)) {
            return Result.ok("添加成功");
        } else {
            return Result.error("添加失败");
        }
    }

    /**
     * 修改任务
     **/
    @PreAuthorize("hasAuthority('task:edit')")
    @ResponseBody
    @RequestMapping("/update")
    public Result update(BatchTask task) {
        if (taskService.update(task)) {
            return Result.ok("修改成功");
        } else {
            return Result.error("修改失败");
        }
    }


    /**
     * 删除任务
     *
     * @param taskId
     * @return
     */
    @PreAuthorize("hasAuthority('task:delete')")
    @ResponseBody
    @RequestMapping("/delete")
    public Result delete(String taskId) {
        if (taskService.delete(taskId)) {
            return Result.ok("删除成功");
        } else {
            return Result.error("删除失败");
        }
    }
}
