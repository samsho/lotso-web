package com.lotso.web.module.batch.service;

import com.lotso.web.common.ReqParam;
import com.lotso.web.common.Result;
import com.lotso.web.common.exception.BusinessException;
import com.lotso.web.common.exception.ParameterException;
import com.lotso.web.module.batch.entity.BatchTask;
import java.util.List;

/**
 * ClassName: IBatchJobGroupService
 * Description:
 * Date: 2018/9/10 17:12 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public interface IBatchTaskService {

    /**
     * 分页列表
     *
     * @param reqParam
     * @return
     */
    Result<BatchTask> list(ReqParam reqParam);


    /**
     * 列表
     *
     * @return
     */
    List<BatchTask> list();

    /**
     * 添加
     * @param batchTask
     * @return
     */
    boolean add(BatchTask batchTask) throws BusinessException;

    /**
     * 修改
     * @param batchTask
     * @return
     */
    boolean update(BatchTask batchTask) throws ParameterException;

    /**
     * 删除
     * @param taskId
     * @return
     */
    boolean delete(String taskId);
}
