package com.lotso.web.module.batch.mapper;

import com.lotso.web.module.base.IBaseMapper;
import com.lotso.web.module.batch.entity.BatchScheduleParam;
import org.springframework.stereotype.Repository;

/**
 * ClassName: IBatchScheduleParamMapper
 * Description:
 * Date: 2018/9/15 10:21 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Repository
public interface IBatchScheduleParamMapper extends IBaseMapper<BatchScheduleParam> {
}
