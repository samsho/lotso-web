package com.lotso.web.module.system.mapper;

import com.lotso.web.module.base.IBaseMapper;
import com.lotso.web.module.system.entity.Dict;
import org.springframework.stereotype.Repository;

/**
 * ClassName: IDictMapper
 * Description: 字典
 * Date: 2018/9/2 9:16 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Repository
public interface IDictMapper extends IBaseMapper<Dict> {

}
