package com.lotso.web.module.system.controller;

import com.lotso.web.common.ReqParam;
import com.lotso.web.common.Result;
import com.lotso.web.module.system.entity.Dict;
import com.lotso.web.module.system.entity.Role;
import com.lotso.web.module.system.entity.User;
import com.lotso.web.module.system.service.impl.DictServiceImpl;
import org.apache.catalina.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author yang@dehong
 * Description: 字典控制器
 * 2018-09-13 19:36
 */

@Controller
@RequestMapping("/system/dict")
public class DictController {

    @Autowired
    DictServiceImpl dictService;

    @PreAuthorize("hasAuthority('dict:view')")
    @GetMapping
    public String dict() {
        return "module/system/dict";
    }

    /**
     * 查询字典列表
     */
    @PreAuthorize("hasAuthority('dict:view')")
    @ResponseBody
    @GetMapping("/list")
    public Result<Dict> list(ReqParam reqParam) {
        return dictService.list(reqParam);
    }

    /**
     * 进入字典添加或者修改页面
     *
     * @param model
     * @return
     */
    @RequestMapping("/editForm")
    public String addDict(Model model) {
        List<Dict> dicts = dictService.list();
        model.addAttribute("dict", dicts);
        return "module/system/dict_form";
    }

    /**
     * 添加字典
     **/
    @PreAuthorize("hasAuthority('dict:add')")
    @ResponseBody
    @RequestMapping("/add")
    public Result add(Dict dict) {
        if (dictService.add(dict)) {
            return Result.ok("添加成功");
        } else {
            return Result.error("添加失败");
        }
    }

    /**
     * 修改用户
     **/
    @PreAuthorize("hasAuthority('dict:edit')")
    @ResponseBody
    @RequestMapping("/update")
    public Result update(Dict dict) {
        if (dictService.update(dict)) {
            return Result.ok("修改成功");
        } else {
            return Result.error("修改失败");
        }
    }

    /**
     * 删除任务
     * @param dictId
     * @return
     */
    @ResponseBody
    @RequestMapping("/delete")
    public Result delete(String dictId) {
        if (dictService.delete(dictId)) {
            return Result.ok("删除成功");
        } else {
            return Result.error("删除失败");
        }
    }

}
