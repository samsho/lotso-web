package com.lotso.web.module.system.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.lotso.web.module.base.AbstractDataEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


/**
 * ClassName: User
 * Description: 角色表
 * Date: 2018/9/2 7:51 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Setter
@Getter
@TableName("t_sys_role")
public class Role extends AbstractDataEntity {

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 描述
     */
    private String comments;

    /**
     * 权限列表
     */
    @TableField(exist = false)
    private List<Menu> authorities;

    public Role() {
    }

    public Role(String id) {
        super(id);
    }
}