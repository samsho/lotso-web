package com.lotso.web.module.system.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.lotso.web.module.base.AbstractDataEntity;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Objects;

/**
 * ClassName: Menu
 * Description: 菜单表
 * Date: 2018/9/2 7:51 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Getter
@Setter
@TableName("t_sys_menu")
public class Menu extends AbstractDataEntity {

    public static final int IS_MENU = 0;
    public static final int IS_BUTTON = 1;
    public static final String ROOT_ID = "-1";

    /**
     * 权限名称
     */
    private String menuName;

    /**
     * 权限标识（如果为空不会添加在权限列表中）
     */
    private String authority;

    /**
     * 菜单url
     */
    private String menuUrl;

    /**
     * 上级菜单
     */
    private String parentId;

    /**
     * 菜单还是按钮 0-菜单 1-按钮
     * （菜单会显示在侧导航，按钮不会显示在侧导航，只要url不是空，都会作为权限标识）
     */
    private Integer isMenu;

    /**
     * 排序号
     */
    private Integer sort;

    /**
     * 菜单图标
     */
    private String menuIcon;


    /**
     * 排序：方便展示
     * -- 父节点1
     * -- 子节点1-1
     * -- 子节点1-2
     * -- 父节点2
     * -- 子节点2-1
     * -- 子节点2-2
     *
     * @param target
     * @param source
     * @param parentId
     * @param cascade
     */
    public static void sortList(List<Menu> target, List<Menu> source, String parentId, boolean cascade) {
        for (Menu menu : source) {
            if (Objects.equals(parentId, menu.getParentId())) {
                target.add(menu);
                if (cascade) {
                    // 判断是否还有子节点, 有则继续获取子节点
                    for (Menu child : source) {
                        if (Objects.equals(menu.getId(), child.getParentId())) {
                            sortList(target, source, menu.getId(), true);
                            break;
                        }
                    }
                }
            }
        }
    }
}
