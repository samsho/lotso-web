package com.lotso.web.module.system.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.lotso.web.module.base.AbstractBaseEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * ClassName: UserRole
 * Description: 用户角色关联表
 * Date: 2018/9/2 7:51 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Setter
@Getter
@TableName("t_sys_user_role")
public class UserRole extends AbstractBaseEntity {

    /**
     * 用户id
     */
    private String userId;

    /**
     * 角色id
     */
    private String roleId;

    /**
     * 角色名称
     */
    @TableField(exist = false)
    private String roleName;
}