package com.lotso.web.module.system.mapper;

import com.lotso.web.module.base.IBaseMapper;
import com.lotso.web.module.system.entity.Role;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ClassName:
 * Description:
 * Date: 2018/9/2 9:16 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Repository
public interface IRoleMapper extends IBaseMapper<Role> {

    /**
     * 通过用户ID获取其用户所有的角色列表
     *
     * @param userId
     * @return
     */
    List<Role> selectByUserId(String userId);
}
