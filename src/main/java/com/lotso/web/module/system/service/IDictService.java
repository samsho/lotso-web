package com.lotso.web.module.system.service;

import com.lotso.web.common.ReqParam;
import com.lotso.web.common.Result;
import com.lotso.web.common.exception.BusinessException;
import com.lotso.web.common.exception.ParameterException;
import com.lotso.web.module.batch.entity.BatchSchedule;
import com.lotso.web.module.system.entity.Dict;
import com.lotso.web.module.system.entity.User;

import java.util.List;

/**
 * ClassName: AuthoritiesService
 * Description: 权限业务
 * Date: 2018/9/2 9:16 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public interface IDictService {

    /**
     * 获取字典类型，获取其所有字典列表
     *
     * @param type
     * @return
     */
    List<Dict> listByType(String type);

    /**
     * 根据条件查询字典列表
     *
     * @param reqParam 请求参数对象
     * @return
     */
    Result<Dict> list(ReqParam reqParam);

    /**
     * 根据条件查询字典列表
     *
     *
     * @return
     */
    List<Dict> list();

    /**
     * 添加
     *
     * @param dict
     * @return
     */
    boolean add(Dict dict) throws BusinessException;


    /**
     * 修改
     *
     * @param dict
     * @return
     */
    boolean update(Dict dict) throws ParameterException;

    /**
     * s删除
     * @param dictid
     * @return
     * @throws ParameterException
     */

    boolean delete(String dictid) throws ParameterException;


}
