package com.lotso.web.module.system.controller;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import com.lotso.web.common.ReqParam;
import com.lotso.web.common.Result;
import com.lotso.web.module.system.entity.Menu;
import com.lotso.web.module.system.entity.Role;
import com.lotso.web.module.system.service.impl.MenuServiceImpl;
import com.lotso.web.module.system.service.impl.RoleServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * ClassName: RoleController
 * Description: 角色管理
 * Date: 2017/12/27 15:36 【需求编号】
 *
 * @author Shaom
 * @version V1.0.0
 */
@Controller
@RequestMapping("/system/role")
public class RoleController {

    @Autowired
    private RoleServiceImpl roleService;
    @Autowired
    private MenuServiceImpl menuService;

    /**
     * 进入角色展示页面
     *
     * @return
     */
    @PreAuthorize("hasAuthority('role:view')")
    @RequestMapping()
    public String role() {
        return "module/system/role";
    }

    /**
     * 查询所有角色（关键字搜索）
     **/
    @PreAuthorize("hasAuthority('role:view')")
    @ResponseBody
    @RequestMapping("/list")
    public Result<Role> list(ReqParam reqParam) {
        return roleService.list(reqParam);
    }

    /**
     * 添加角色操作
     **/
    @PreAuthorize("hasAuthority('role:add')")
    @ResponseBody
    @RequestMapping("/add")
    public Result add(Role role) {
        if (roleService.add(role)) {
            return Result.ok("添加成功");
        } else {
            return Result.error("添加失败");
        }
    }


    /**
     * 修改角色
     **/
    @PreAuthorize("hasAuthority('role:edit')")
    @ResponseBody
    @RequestMapping("/update")
    public Result update(Role role) {
        if (roleService.update(role)) {
            return Result.ok("修改成功！");
        } else {
            return Result.error("修改失败！");
        }
    }

    /**
     * 删除角色
     **/
    @PreAuthorize("hasAuthority('role:delete')")
    @ResponseBody
    @RequestMapping("/delete")
    public Result delete(String roleId) {
        if (roleService.delete(roleId)) {
            return Result.ok("删除成功");
        }
        return Result.error("删除失败");
    }

    /**
     * 获取角色权限树
     */
    @ResponseBody
    @GetMapping("/authTree")
    public List<Map<String, Object>> authTree(String roleId) {
        List<Menu> roleAuths = menuService.listByRoleId(roleId);
        List<Menu> allAuths = menuService.list();
        List<Map<String, Object>> authTrees = new ArrayList<>();
        for (Menu one : allAuths) {
            Map<String, Object> authTree = Maps.newHashMap();
            authTree.put("id", one.getId());
            authTree.put("name", one.getMenuName() + " " + Strings.nullToEmpty(one.getAuthority()));
            authTree.put("pId", one.getParentId());
            authTree.put("open", true);
            authTree.put("checked", false);
            for (Menu temp : roleAuths) {
                if (temp.getId().equals(one.getId())) {
                    authTree.put("checked", true);
                    break;
                }
            }
            authTrees.add(authTree);
        }
        return authTrees;
    }

    /**
     * 修改角色权限
     */
    @PreAuthorize("hasAuthority('role:auth')")
    @ResponseBody
    @PostMapping("/updateRoleAuth")
    public Result updateRoleAuth(String roleId, String authIds) {
        if (menuService.updateRoleAuth(roleId, JSON.parseArray(authIds, String.class))) {
            return Result.ok("修改成功");
        }
        return Result.error("修改失败");
    }
}
