package com.lotso.web.module.system.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * ClassName: DemoController
 * Description:
 * Date: 2019/7/12 14:05 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Controller
@RequestMapping("/system/demo")
public class DemoController {

    /**
     * 进入用户展示页面
     *
     * @return
     */
    @GetMapping("1")
    public String user() {
        return "module/system/user";
    }


    /**
     * 进入角色展示页面
     *
     * @return
     */
    @RequestMapping("2")
    public String role() {
        return "module/system/role";
    }
}
