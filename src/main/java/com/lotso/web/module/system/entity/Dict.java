package com.lotso.web.module.system.entity;

import com.baomidou.mybatisplus.annotations.TableName;
import com.lotso.web.module.base.AbstractDataEntity;
import lombok.Getter;
import lombok.Setter;

/**
 * ClassName: Dict
 * Description: 字典表
 * Date: 2018/9/13 11:52 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Getter
@Setter
@TableName("t_sys_dict")
public class Dict extends AbstractDataEntity {
    /**
     * 字典值
     */
    private String value;
    /**
     * 字典中文意思
     */
    private String label;
    /**
     * 字典值类型
     */
    private String type;
    /**
     * 描述
     */
    private String description;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 父类字典
     */
    private String parentId;
}
