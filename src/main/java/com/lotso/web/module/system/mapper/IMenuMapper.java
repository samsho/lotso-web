package com.lotso.web.module.system.mapper;

import com.lotso.web.module.base.IBaseMapper;
import com.lotso.web.module.system.entity.Menu;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * ClassName:
 * Description:
 * Date: 2018/9/2 9:16 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Repository
public interface IMenuMapper extends IBaseMapper<Menu> {

    /**
     * 获取登录用户菜单信息
     *
     * @param userId
     * @return
     */
    List<Menu> listByUserId(String userId);

    /**
     * 根据角色ID 获取该角色的权限列表
     *
     * @param roleId
     * @return
     */
    List<Menu> listByRoleId(String roleId);


}
