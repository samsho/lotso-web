package com.lotso.web.common.log;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableScheduledFuture;
import com.google.common.util.concurrent.ListeningScheduledExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.lotso.web.common.utils.UserAgentUtil;
import com.lotso.web.common.utils.UserUtil;
import com.lotso.web.module.monitor.entity.Log;
import com.lotso.web.module.monitor.service.ILogService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * ClassName: ControllerAspect
 * Description: 日志记录器
 * Date: 2016/4/7 15:09
 *
 * @author SAM SHO
 * @version V1.0
 */
@Slf4j
//@Component
//@Aspect
public class LogAspect {

    private final int poolSize = 2;

    private ListeningScheduledExecutorService executorService;

    private BlockingQueue<Log> queue;

    private int batchSize = 100;

    @Resource
    private ILogService logService;

    public LogAspect() {
        executorService = MoreExecutors.listeningDecorator(
                Executors.newScheduledThreadPool(poolSize,
                        new ThreadFactoryBuilder().setNameFormat("log-thread-%d").build()));
        queue = new LinkedBlockingQueue<>();
    }

    @PostConstruct
    public void startSchedule() {
        for (int i = 0; i < poolSize; i++) {
            ListenableScheduledFuture<?> future = executorService
                    .scheduleWithFixedDelay(getRunnable(), 10, 10, TimeUnit.SECONDS);
            // 添加监控
            Futures.addCallback(future, new FutureCallback<Object>() {
                @Override
                public void onSuccess(@Nullable Object result) {

                }

                @Override
                public void onFailure(Throwable t) {
                    executorService.scheduleWithFixedDelay(LogAspect.this.getRunnable(), 10, 10, TimeUnit.SECONDS);
                }
            }, executorService);
        }
    }

    @PreDestroy
    public void shutdown() {
        MoreExecutors.shutdownAndAwaitTermination(executorService, 5, TimeUnit.SECONDS);
    }

    @Pointcut("@within(org.springframework.stereotype.Controller)")
    public void viewMethod() {

    }

    @Around("viewMethod()")
    public Object doBefore(ProceedingJoinPoint joinPoint) throws Throwable {
        Signature signature = joinPoint.getSignature();

        if (signature instanceof MethodSignature) {

            HttpServletRequest request = getRequest();
            HttpSession session = request.getSession();
            if (!Objects.isNull(session)) {
                // 只记录已经登录的用户
                String username = UserUtil.getPrincipalName();
                if (!Strings.isNullOrEmpty(username)) {

                    MethodSignature mtdSign = (MethodSignature) signature;
                    Object[] args = joinPoint.getArgs();
                    Method method = mtdSign.getMethod();
                    LogAnnotation annotation = method.getAnnotation(LogAnnotation.class);

                    Log logEntity = new Log();
                    logEntity.setUsername(username);
                    StringBuilder stringBuilder = getParams(mtdSign, args);
                    logEntity.setArgs(stringBuilder.toString());
                    logEntity.setUrl(request.getRequestURI());

                    UserAgentUtil agentGetter = new UserAgentUtil(request);
                    logEntity.setOsName(agentGetter.getOS());
                    logEntity.setDevice(agentGetter.getDevice());
                    logEntity.setBrowserType(agentGetter.getBrowser());
                    logEntity.setIp(agentGetter.getIpAddr());

                    if (annotation != null) {
                        String action = annotation.type().getDesc() + annotation.desc();
                        logEntity.setAction(action);
                    } else {
                        logEntity.setAction(request.getMethod());
                    }

                    logEntity.setIp(request.getRemoteAddr());
                    queue.add(logEntity);
                }
            }

        }

        return joinPoint.proceed();
    }

    @AfterThrowing(value = "viewMethod()", throwing = "ex")
    public void logError(Throwable ex) {
        HttpServletRequest request = getRequest();
        HttpSession session = request.getSession();
        if (!Objects.isNull(session)) {

            String username = UserUtil.getPrincipalName();
            if (!Strings.isNullOrEmpty(username)) {
                Log logEntity = new Log();
                logEntity.setUsername(username);
                logEntity.setUrl(request.getRequestURI());
                logEntity.setIp(request.getRemoteAddr());

                UserAgentUtil agentGetter = new UserAgentUtil(request);
                logEntity.setOsName(agentGetter.getOS());
                logEntity.setDevice(agentGetter.getDevice());
                logEntity.setBrowserType(agentGetter.getBrowser());
                logEntity.setIp(agentGetter.getIpAddr());

                try (StringWriter stringWriter = new StringWriter()) {
                    PrintWriter p = new PrintWriter(stringWriter);
                    ex.printStackTrace(p);
                    logEntity.setException(stringWriter.toString());
                } catch (IOException e) {
                    log.error("将错误日志转成string时出错", e);
                }
                queue.add(logEntity);
            }
        }
    }

    private StringBuilder getParams(MethodSignature mtdSign, Object[] args) {
        String[] params = mtdSign.getParameterNames();
        Class[] paramTypes = mtdSign.getParameterTypes();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < args.length; i++) {
            Class type = paramTypes[i];
            if (!HttpServletRequest.class.isAssignableFrom(type)
                    && !ModelMap.class.isAssignableFrom(type)
                    && !HttpServletResponse.class.isAssignableFrom(type)
                    && !WebDataBinder.class.isAssignableFrom(type)) {
                final Object arg = args[i];
                String s;
                if (arg == null) {
                    s = "null";
                } else if (arg instanceof String) {
                    s = "\"" + arg + "\"";
                } else if (type.getAnnotation(LogEntity.class) != null) {
                    // 如果是数据库的类，记录下来
                    s = JSON.toJSONString(arg);
                } else {
                    s = arg.toString();
                }
                stringBuilder.append(params[i]).append("=").append(s).append(";");
            }
        }
        return stringBuilder;
    }

    private HttpServletRequest getRequest() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder
                .currentRequestAttributes();
        return attributes.getRequest();
    }

    private Runnable getRunnable() {
        return () -> {
            if (!queue.isEmpty()) {
                final List<Log> entities = Lists.newArrayListWithExpectedSize(batchSize);
                queue.drainTo(entities, batchSize);
                log.info(JSON.toJSONString(entities));
                logService.addBatch(entities);
            }
        };

    }
}
