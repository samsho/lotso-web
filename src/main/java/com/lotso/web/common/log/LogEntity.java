package com.lotso.web.common.log;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * ClassName: Entity
 * Description: 数据实体或者POJO的标记
 * Date: 2017/10/25 10:44
 *
 * @author Shaom
 * @version V1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface LogEntity {
}
