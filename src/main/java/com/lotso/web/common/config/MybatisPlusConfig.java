package com.lotso.web.common.config;

import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * ClassName: MybatisPlusConfig
 * Description: MybatisPlusConfig 配置
 * Date: 2018/8/30 10:51 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Configuration
@MapperScan(value = MybatisPlusConfig.SCAN_PATH, annotationClass = Repository.class)
@EnableTransactionManagement
public class MybatisPlusConfig {


    static final String SCAN_PATH = "com.lotso.web.**.mapper";

    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }
}