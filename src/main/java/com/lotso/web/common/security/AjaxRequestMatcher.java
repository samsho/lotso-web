package com.lotso.web.common.security;

import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * ClassName: AjaxRequestMatcher
 * Description: Ajax 访问匹配器
 * Date: 2017/12/27 15:40 【需求编号】
 *
 * @author Shaom
 * @version V1.0.0
 */
public final class AjaxRequestMatcher implements RequestMatcher {

    private static final String AJAX_REQUEST = "XMLHttpRequest";

    @Override
    public boolean matches(HttpServletRequest request) {
        String requestType = request.getHeader("X-Requested-With");
        return Objects.equals(AJAX_REQUEST, requestType);
    }
}