/**
 * ClassName: package-info
 * Description: Spring Security 权限
 * Date: 2018/10/29 9:55 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
package com.lotso.web.common.security;