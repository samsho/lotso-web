package com.lotso.web.common.exception;


/**
 * ClassName: ParameterException
 * Description: 参数异常
 * Date: 2018/8/30 10:51 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class ParameterException extends AbstractException {
    private static final long serialVersionUID = 7993671808524980055L;

    public ParameterException() {
        super();
    }

    public ParameterException(String message) {
        super(message);
    }

    public ParameterException(Integer code, String message) {
        super(code, message);
    }

    @Override
    public Integer getCode() {
        Integer code = super.getCode();
        if (code == null) {
            code = 400;
        }
        return code;
    }

    @Override
    public String getMessage() {
        String message = super.getMessage();
        if (message == null) {
            message = "参数错误";
        }
        return message;
    }
}
